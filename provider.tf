provider "google" {
  credentials = "${file("serviceaccount.json")}"
  project     = "spatial-subject-200921"
  region      = "europe-west1"
}

resource "google_container_cluster" "primary" {
    name      = "whyfor"
    zone      = "europe-west1"
    initial_node_count = 1
}

# The following outputs allow authentication and connectivity to the GKE Cluster.
output "client_certificate" {
  value = "${base64decode(google_container_cluster.primary.master_auth.0.client_certificate)}"
}

output "client_key" {
  value = "${base64decode(google_container_cluster.primary.master_auth.0.client_key)}"
}

output "cluster_ca_certificate" {
  value = "${base64decode(google_container_cluster.primary.master_auth.0.cluster_ca_certificate)}"
}
