# Create GKE cluster via terraform

Simple example for terraform usage


## Prepare

- Install terraform binary OR use gitlab runner (dind) with SERVICEACCOUNT env
- Go to GCE cloud console
  - iAM and roles
  - left menu -> serviceaccounts
  - create new
  - super-user role (project editor), create and download json file and place as "serviceaccount.json" (or fill env)
  - get "project_id" field from json, replace at next step
- replace my hardcoded project id via
  - `sed -i "s/spatial-subject-200921/${YOUR_PROJECT_NAME}/g" *`
- Deploy via
  - `terraform validate`
  - `terraform plan -out "planfile" -no-color`
  - `terraform apply -input=false "planfile" -no-color`
- and DESTROY via
  - `terraform plan -out "planfile" -no-color -destroy`
  - `terraform apply -input=false "planfile" -no-color`
